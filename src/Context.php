<?php

namespace Blazing\Reseller\Api;

use Blazing\Common\RestApiRequestHandler\Exception\RequestsException;
use Blazing\Common\RestApiRequestHandler\GenericApiContext;

class Context extends GenericApiContext
{

    protected $userId;

    /**
     * Get userId

     *
*@param bool $throw
     * @return mixed
     * @throws RequestsException
     */
    public function getUserId($throw = false)
    {
        if (!$this->userId and $throw) {
            throw new RequestsException('Context Error: userId is not defined');
        }

        return $this->userId;
    }

    /**
     * Set userId
     *
     * @param mixed $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }
}