<?php

namespace Blazing\Reseller\Api\Api;

use Blazing\Reseller\Api\Api\Entity\PackageEntity;

class PackagesApi extends AbstractApi
{
    public function getAll($source = false, $status = false, $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->get('/user/{userId}/packages', [
            'userId' => $userId,
            'source' => $source,
            'status' => $status
        ]);
    }

    public function deleteAll($source = false, $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->delete('/user/{userId}/packages', ['userId' => $userId, 'source' => $source]);
    }

    public function get($country, $category, $source = false, $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->get('/user/{userId}/packages/find', [
            'userId' => $userId,
            'source' => $source,
            'country' => $country,
            'category' => $category,
        ]);
    }

    public function add(PackageEntity $package, $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->post('/user/{userId}/packages/{source}', [
            'userId'   => $userId,
            'source'   => $package->getSource(),
            'country'  => $package->getCountry(),
            'category' => $package->getCategory(),
            'ports'    => $package->getPorts(),
            'status'   => $package->getStatus()
        ]);
    }

    public function update(PackageEntity $package, $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->put('/user/{userId}/packages/{source}', [
            'userId'   => $userId,
            'source'   => $package->getSource(),
            'country'  => $package->getCountry(),
            'category' => $package->getCategory(),
            'ports'    => $package->getPorts(),
            'status'   => $package->getStatus()
        ]);
    }

    public function delete($country, $category, $source, $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->delete('/user/{userId}/packages/{source}', [
            'userId' => $userId,
            'source' => $source,
            'country' => $country,
            'category' => $category,
        ]);
    }
}
