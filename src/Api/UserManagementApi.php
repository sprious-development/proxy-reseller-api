<?php

namespace Blazing\Reseller\Api\Api;

class UserManagementApi extends AbstractApi
{
    public function getUserByBillingId($source, $id)
    {
        return $this->api->request()->get('/users/find/byBillingId/{source}/{id}', ['source' => $source, 'id' => $id]);
    }

    public function findUserByLoginOrEmail($loginOrEmail)
    {
        return $this->api->request()->get('/users/find/byLoginOrEmail/{loginOrEmail}', ['loginOrEmail' => $loginOrEmail]);
    }

    public function upsertUser($source, $id, $email)
    {
        return $this->api->request()->post('/users/upsert/{source}/{id}', ['source' => $source, 'id' => $id, 'email' => $email]);
    }
}