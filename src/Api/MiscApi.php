<?php

namespace Blazing\Reseller\Api\Api;

class MiscApi extends AbstractApi
{
    public function getLocationsAvailability($userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->get('/misc/proxy/locations/{userId}', ['userId' => $userId]);
    }

    public function getProxyTypesAllowedAction()
    {
        return $this->api->request()->get('/misc/proxy/types');
    }
}