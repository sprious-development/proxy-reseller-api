<?php

namespace Blazing\Reseller\Api\Api;

use Blazing\Common\RestApiRequestHandler\Api\AbstractApi as BaseApi;
use Blazing\Reseller\Api\Api;

/**
 * Class AbstractApi
 *
 * @package Blazing\Reseller\Api\Api
 * @property Api $api
 */
abstract class AbstractApi extends BaseApi
{
}
