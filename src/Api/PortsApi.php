<?php

namespace Blazing\Reseller\Api\Api;

class PortsApi extends AbstractApi
{

    public function getAll($country = [], $category = [], $sortBy = [], $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->get('/user/{userId}/ports', [
            'userId'   => $userId,
            'country'  => $country,
            'category' => $category,
            'sortBy'   => $sortBy
        ]);
    }

    public function getAllocation($country = [], $category = [], $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->get('/user/{userId}/ports/allocation', [
            'userId'   => $userId,
            'country'  => $country,
            'category' => $category,
        ]);
    }

    public function setAllocation($country, $category, array $allocation, $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->post('/user/{userId}/ports/allocation', [
            'userId'     => $userId,
            'country'    => $country,
            'category'   => $category,
            'allocation' => $allocation
        ]);
    }

    public function setRotationTime($portId, $time, $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->post('/user/{userId}/ports/{id}/rotationTime', [
            'userId' => $userId,
            'id' => $portId,
            'time'   => $time
        ]);
    }

    public function getAvailableReplacements($userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->get('/user/{userId}/ports/replacements', ['userId' => $userId]);
    }

    public function setPendingReplace($ip, $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->post('/user/{userId}/ports/replacements/{ip}', ['userId' => $userId, 'ip' => $ip]);
    }

    public function setPendingReplaceMultiple(array $ipList, $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->post('/user/{userId}/ports/replacements', ['userId' => $userId, 'ip' => $ipList]);
    }
}