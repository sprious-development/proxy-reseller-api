<?php

namespace Blazing\Reseller\Api\Api;

class MtaApi extends AbstractApi
{
    public function isUserWhitelisted($userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->get('/user/{userId}/mta/isWhitelisted', ['userId' => $userId]);
    }

    public function isUserIpTrusted($ip, $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->get('/user/{userId}/mta/ip/isTrusted', ['userId' => $userId, 'ip' => $ip]);
    }

    public function upsertUserIp($ip, $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->post('/user/{userId}/mta/ip', ['userId' => $userId, 'ip' => $ip]);
    }

    public function storeOtp($type, $code, $lifetime, $attempts, $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->post('/user/{userId}/mta/otp/{type}', [
            'userId'   => $userId,
            'type'     => $type,
            'code'     => $code,
            'lifetime' => $lifetime,
            'attempts' => $attempts
        ]);
    }

    public function deleteOtp($type, $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->delete('/user/{userId}/mta/otp/{type}', ['userId' => $userId, 'type' => $type]);
    }

    public function isOtpExists($type, $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->get('/user/{userId}/mta/otp/isExists/{type}', ['userId' => $userId, 'type' => $type]);
    }

    public function getOtp($type, $hasAttempts = true, $code = false, $notExpired = true, $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->get('/user/{userId}/mta/otp/{type}', [
            'userId'      => $userId,
            'type'        => $type,
            'hasAttempts' => $hasAttempts,
            'code'        => $code,
            'notExpired'  => $notExpired
        ]);
    }

    public function decrementOtpAttempts($type, $userId = null)
    {
        $userId or $userId = $this->api->getContext()->getUserId(true);

        return $this->api->request()->post('/user/{userId}/mta/otp/decrement/{type}', ['userId' => $userId, 'type' => $type]);
    }
}
