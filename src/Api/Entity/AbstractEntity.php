<?php

namespace Blazing\Reseller\Api\Api\Entity;

abstract class AbstractEntity
{
    public static function construct()
    {
        return new static();
    }
}
