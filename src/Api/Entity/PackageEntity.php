<?php

namespace Blazing\Reseller\Api\Api\Entity;

class PackageEntity extends AbstractEntity
{

    const STATUS_ACTIVE = 'active';
    const STATUS_SUSPENDED = 'suspended';

    protected $country;

    protected $category;

    protected $source = 'whmcs';

    protected $status = self::STATUS_ACTIVE;

    protected $ports;

    public function __construct($country = null, $category = null, $source = null)
    {
        if ($country) {
            $this->setCountry($country);
        }

        if ($category) {
            $this->setCategory($category);
        }

        if ($source) {
            $this->setSource($source);
        }
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set category
     *
     * @param string $category
     * @return $this
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get source
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set source
     *
     * @param string $source
     * @return $this
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get ports
     *
     * @return int
     */
    public function getPorts()
    {
        return $this->ports;
    }

    /**
     * Set ports
     *
     * @param int $ports
     * @return $this
     */
    public function setPorts($ports)
    {
        $this->ports = (int) $ports;

        return $this;
    }
}
