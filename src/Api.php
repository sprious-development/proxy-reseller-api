<?php

namespace Blazing\Reseller\Api;

use Blazing\Common\RestApiRequestHandler\AbstractApi;

/**
 * Class Api
 *
 * @package Blazing\Reseller\Api
 * @method Context getContext()
 * @method Api\UserApi user()
 * @method Api\UserManagementApi userManagement()
 * @method Api\PackagesApi packages()
 * @method Api\PortsApi ports()
 * @method Api\MiscApi misc()
 * @method Api\MtaApi mta()
 */
class Api extends AbstractApi
{
    protected $map = [
        'user' => Api\UserApi::class,
        'userManagement' => Api\UserManagementApi::class,
        'packages' => Api\PackagesApi::class,
        'ports' => Api\PortsApi::class,
        'misc' => Api\MiscApi::class,
        'mta' => Api\MtaApi::class,
    ];

    public function clearContext()
    {
        $this->context = new Context();
    }
}