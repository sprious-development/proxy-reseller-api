<?php

namespace Blazing\Reseller\Api;

use Blazing\Common\RestApiRequestHandler\GenericApiConfiguration;

class Configuration extends GenericApiConfiguration
{

    protected $protocol = 'http';

    protected $host = 'proxy.blazingseollc.com';

    protected $url = '/api/reseller/v2.0';
}