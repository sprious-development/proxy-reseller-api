FORMAT: 1A
HOST: http://proxy.blazingseollc.com/api/reseller/v2.0

# Blazing SEO Proxy Reseller API v2.0

Documentation - http://docs.blazingseoproxyresellerapiv20.apiary.io/

Documentation editor - https://app.apiary.io/blazingseoproxyresellerapiv20/editor

# Data Structures

## AuthToken (object)

+ Auth-Token (string) - Your reseller token
+ text (string) - text of the message

# Group User Management

## Find user id by billing id [/users/find/byBillingId/{source}/{id}]

### Example [GET]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - source (string, required) - Currently allowed values: whmcs/amember
    - id (number, required) - User id in correspondent billing

+ Response 200 (application/json)

        {
            "userId": "1"
        }

+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "Unable to find user",
            "code": "NO_USER_FOUND"
        }

## Find user by email or login [/users/find/byLoginOrEmail/{loginOrEmail}]

### Example [GET]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - loginOrEmail (string, required) - test

+ Response 200 (application/json)

        {
            "userId": "1",
            "email": "and.webdev@gmail.com",
            "login": "andwebdev",
            "rotate_30": "1",
            "rotate_ever": "0",
            "authType": "PW",
            "apiKey": "********",
            "sneakerLocation": "LA",
            "whmcsId": "2",
            "amemberId": "3"
        }

+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "Unable to find user",
            "code": "NO_USER_FOUND"
        }

## Insert or update user - on authorization [/users/upsert/{source}/{id}]

### Example [POST]

+ Headers

    Auth-Token: YourResellerApiToken

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - source (string, required) - Currently allowed values: whmcs/amember
    - id (number, required) - User id
    - email (string, required) - User email, updates the current one; this is needed to keep user email in sync with billing id-s

+ Response 200 (application/json)

        {
            "userId": "1",
            "email": "and.webdev@gmail.com",
            "login": "andwebdev",
            "rotate_30": "1",
            "rotate_ever": "0",
            "authType": "PW",
            "apiKey": "********",
            "sneakerLocation": "LA",
            "whmcsId": "2",
            "amemberId": "3"
        }

+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "E-mail is invalid",
            "code": "UNKNOWN"
        }
        

# Group User

## Details [/user/{userId}/details]

### Get [GET]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    
+ Response 200 (application/json)

        {
            "userId": "1",
            "email": "and.webdev@gmail.com",
            "login": "andwebdev",
            "rotate_30": "1",
            "rotate_ever": "0",
            "authType": "PW",
            "apiKey": "********",
            "sneakerLocation": "LA",
            "whmcsId": "2",
            "amemberId": "3"
        }

+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "User \"0\" is not found!",
            "code": "UNKNOWN"
        }

### Update [POST]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    - data (object)
        - authType (string) - IP (by ip) or PW (by password/token)
        - rotate_ever (bool) - should the system rotate user proxy in case of it is dead
        - rotate_30 (bool) - should the system rotate
    
+ Response 200 (application/json)

        {
            "status": "ok"
        }

+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "No valid options passed!",
            "code": "UNKNOWN"
        }

### Update sneaker location [POST /user/{userId}/details/sneakerLocation]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    - location (string, required) - User sneaker location, currently only LA is available
    
+ Response 200 (application/json)

        {
            "status": "ok"
        }

+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "Invalid Location Passed",
            "code": "UNKNOWN"
        }
        
## Auth IP [/user/{userId}/details/auth/ip]

### Get list [GET]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    
+ Response 200 (application/json)

    {
        "list": [
            {
                "id": "57633",
                "ip": "1.2.3.4"
            },
            {
                "id": "91038",
                "ip": "4.3.2.1"
            }
        ]
    }
    
### Add IP [POST /user/{userId}/details/auth/ip/{ip}]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    - ip (string, required) - Valid IPv4 address
    
+ Response 200 (application/json)

    {
        "status": "ok",
        "ip": {
            "id": "123",
            "ip": "1.2.3.4"
        }
    }

+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "Duplicate IP address.",
            "code": "UNKNOWN"
        }
        
### Remove IP [DELETE /user/{userId}/details/auth/ip/{ipId}]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    - ipId (number, required) - ip's id
    
+ Response 200 (application/json)

    {
        "status": "ok"
    }
    
# Group Proxy Packages

## Batch operations [/user/{userId}/packages]

### Get all packages [GET]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    - source (string) - only packages related to billing. Available values: whmcs/amember
    
+ Response 200 (application/json)

    {
        "status": "ok",
        "count": 2,
        "list": [
            {
                "country": "us",
                "category": "semi-3",
                "ports": 2,
                "source": "whmcs",
                "id": "1",
                "status": "active"
            },
            {
                "country": "us",
                "category": "dedicated",
                "ports": 9,
                "source": "whmcs",
                "id": "2",
                "status": "active"
            }
        ]
    }
    
+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "No user \"0\" found",
            "code": "UNKNOWN"
        }

### Remove all packages [DELETE]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    - source (string) - only packages related to billing. Available values: whmcs/amember
    
+ Response 200 (application/json)

    {
        "status": "ok",
        "count": 2, // how much has been removed
        "list": [] // actual packages list
    }
    
+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "No user \"0\" found",
            "code": "UNKNOWN"
        }

## Single operation [/]

### Get package [GET /user/{userId}/packages/find]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    - source (string) - only package related to billing. Available values: whmcs/amember
    - country (string, required)
    - category (string, required)
    
+ Response 200 (application/json)

    {
        "item": {
                "country": "us",
                "category": "dedicated",
                "ports": 9,
                "source": "whmcs",
                "id": "2",
                "status": "active"
            }
    }
    
+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "No user \"0\" found",
            "code": "UNKNOWN"
        }

### Add package [POST /user/{userId}/packages/{source}]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    - source (string, required) - package's billing system. Available values: whmcs/amember
    - country (string, required)
    - category (string, required)
    - ports (number, required) - Ports quantity
    - status (string) - Package status. Available values: active/suspended. It's "active" by default
    
+ Response 200 (application/json)

    {
        "status": "ok",
        "count": 1,
        "list": [ // all packages with the added one
            {
                "country": "us",
                "category": "semi-3",
                "ports": 2,
                "source": "whmcs",
                "id": "1",
                "status": "active"
            },
            {
                "country": "us",
                "category": "dedicated",
                "ports": 9,
                "source": "whmcs",
                "id": "2",
                "status": "active"
            }
        ]
    }
    
+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "Can not update package as \"us-unexistent\" package already exists",
            "code": "UNKNOWN"
        }

### Update package [PUT /user/{userId}/packages/{source}]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    - source (string, required) - package's billing system. Available values: whmcs/amember
    - country (string, required)
    - category (string, required)
    - ports (number, required) - Ports quantity
    - status (string) - Package status. Available values: active/suspended
    
+ Response 200 (application/json)

    {
        "status": "ok",
        "count": 1,
        "list": [ // all packages with the added one
            {
                "country": "us",
                "category": "semi-3",
                "ports": 2,
                "source": "whmcs",
                "id": "1",
                "status": "active"
            },
            {
                "country": "us",
                "category": "dedicated",
                "ports": 9,
                "source": "whmcs",
                "id": "2",
                "status": "active"
            }
        ]
    }
    
+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "Can not update package as no \"us-unexistent\" (\"whmcs\") exists",
            "code": "UNKNOWN"
        }

### Remove package [DELETE /user/{userId}/packages/{source}]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    - source (string, required) - package's billing system. Available values: whmcs/amember
    - country (string, required)
    - category (string, required)
    
+ Response 200 (application/json)

    {
        "status": "ok",
        "count": 1,
        "list": [ // all packages with the added one
            {
                "country": "us",
                "category": "semi-3",
                "ports": 2,
                "source": "whmcs",
                "id": "1",
                "status": "active"
            },
            {
                "country": "us",
                "category": "dedicated",
                "ports": 9,
                "source": "whmcs",
                "id": "2",
                "status": "active"
            }
        ]
    }
    
+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "Can not remove package as no \"us-unexistent\" (\"whmcs\") exists",
            "code": "UNKNOWN"
        }

# Group Proxy Ports

## Get all [/user/{userId}/ports]

### Example [GET]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    
+ Response 200 (application/json)

    {
        "status": "ok"
    }
    
+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "No user \"0\" found",
            "code": "UNKNOWN"
        }

## Allocation [/user/{userId}/ports/allocation]

### Get [GET]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    
+ Response 200 (application/json)

    {
        "status": "ok"
    }
    
+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "No user \"0\" found",
            "code": "UNKNOWN"
        }

### Update [POST]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    
+ Response 200 (application/json)

    {
        "status": "ok"
    }
    
+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "No user \"0\" found",
            "code": "UNKNOWN"
        }

## Set rotation time [/user/{userId}/ports/{id}/rotationTime]

### Example [POST]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    
+ Response 200 (application/json)

    {
        "status": "ok"
    }
    
+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "No user \"0\" found",
            "code": "UNKNOWN"
        }

## Replacements [/user/{userId}/ports/replacements]

### Get available [GET]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    
+ Response 200 (application/json)

    {
        "status": "ok"
    }
    
+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "No user \"0\" found",
            "code": "UNKNOWN"
        }

### Replace single proxy [POST /user/{userId}/ports/replacements/{ip}]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    
+ Response 200 (application/json)

    {
        "status": "ok"
    }
    
+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "No user \"0\" found",
            "code": "UNKNOWN"
        }

### Replace multiple proxies [POST]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    
+ Response 200 (application/json)

    {
        "status": "ok"
    }
    
+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "No user \"0\" found",
            "code": "UNKNOWN"
        }

# Group Misc

## Get locations availability [/misc/proxy/locations/{userId}]

### Example [GET]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    
+ Response 200 (application/json)

        {
            "list": {
                "us": {
                    "1": {
                        "state": null,
                        "city": "Mixed",
                        "categories": {
                            "semi-3": 0,
                            "dedicated": 0,
                            "rotating": 0
                        }
                    },
                    "2": {
                        "state": "New York",
                        "city": "Buffalo",
                        "categories": {
                            "semi-3": 419,
                            "dedicated": 190,
                            "rotating": 60
                        }
                    },
                    "7": {
                        "state": "Illinois",
                        "city": "Chicago",
                        "categories": {
                            "semi-3": 1052,
                            "dedicated": 622,
                            "rotating": 100
                        }
                    },
                },
                "de": {
                    "33": {
                        "state": null,
                        "city": "Frankfurt",
                        "categories": {
                            "semi-3": 717,
                            "dedicated": 235,
                            "rotating": 5
                        }
                    }
                },
                "br": {
                    "35": {
                        "state": null,
                        "city": "Mixed",
                        "categories": {
                            "semi-3": 337,
                            "dedicated": 0,
                            "rotating": 145
                        }
                    },
                    "79": {
                        "state": null,
                        "city": "Sao Pao",
                        "categories": {
                            "semi-3": 199,
                            "dedicated": 195,
                            "rotating": 0
                        }
                    }
                },
                "gb": {
                    "36": {
                        "state": null,
                        "city": "Birmingham",
                        "categories": {
                            "semi-3": 0,
                            "dedicated": 0,
                            "rotating": 0
                        }
                    },
                },
                "fr": {
                    "38": {
                        "state": null,
                        "city": "Paris",
                        "categories": {
                            "semi-3": 5,
                            "dedicated": 5,
                            "rotating": 0
                        }
                    }
                }
            }
        }
    
+ Response 500 (application/json)

        {
            "status": "error",
            "error": true,
            "message": "No user \"0\" found",
            "code": "UNKNOWN"
        }

## Get allowed proxy types [/misc/proxy/types]

### Example [GET]

+ Headers

    Auth-Token: YourResellerApiToken
    
+ Response 200 (application/json)

    {
        "list": {
            "dedicated": {
                "us": "us",
                "de": "de",
                "br": "br",
                "gb": "gb",
                "fr": "fr"
            },
            "semi-3": {
                "us": "us",
                "de": "de",
                "br": "br",
                "gb": "gb",
                "fr": "fr"
            },
            "rotating": {
                "us": "us",
                "de": "de",
                "br": "br",
                "gb": "gb",
                "fr": "fr"
            },
            "sneaker": {
                "us": "us"
            },
            "kushang": {
                "us": "us",
                "de": "de",
                "br": "br",
                "gb": "gb",
                "fr": "fr"
            },
            "mapple": {
                "us": "us",
                "de": "de",
                "br": "br",
                "gb": "gb",
                "fr": "fr"
            },
            "google": {
                "us": "us",
                "de": "de",
                "br": "br",
                "gb": "gb",
                "fr": "fr"
            }
        }
    }
    
# Group TFA

## Is user whitelisted - no need of MTA validation [/user/{userId}/mta/isWhitelisted]

### Example [GET]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    
+ Response 200 (application/json)

    {
        "status": "ok",
        "result": true
    }
    
    
## Does user have IP stored [/user/{userId}/mta/ip/isTrusted]

### Example [GET]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    - ip (string, required) - User ip to check
    
+ Response 200 (application/json)

    {
        "status": "ok",
        "result": true
    }
    
## Upsert user ip [/user/{userId}/mta/ip]

### Example [POST]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    - ip (string, required) - User ip to check
    
+ Response 200 (application/json)

    {
        "status": "ok",
        "created": true, // opposite to updated
        "updated": false
    }
    
## Check if OTP is generated [/user/{userId}/mta/otp/isExists/{type}]

### Example [GET]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    - type (string, required) - OTP type
    
+ Response 200 (application/json)

    {
        "status": "ok",
        "result": true,
        "otp": { // is empty if result is false
          "type":"tfa_email",
          "code":"568139",
          "attempts":"5",
          "expiration":"2017-08-18 13:03:12"
        }
    }
    
## Manage OTPs [/user/{userId}/mta/otp/{type}]

### Get the latest OTP by criteria [GET]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    - type (string, required) - OTP type
    - hasAttempts (bool) - has more left attempts (default true)
    - code (string) - check if exact code is exist (default false)
    - nonExpired (bool) - should be not expired (default true)
    
+ Response 200 (application/json)

    {
        "status": "ok",
        "otp": {
          "type":"tfa_email",
          "code":"568139",
          "attempts":"5",
          "expiration":"2017-08-18 13:03:12"
        }
    }
    
### Add a new code [POST]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    - type (string, required) - OTP type
    - code (string, required) - OTP code
    - lifetime (number, required) - OTP lifetime in seconds
    - attempts (number, required) - how many attempts is allowed
    
+ Response 200 (application/json)

    {
        "status": "ok",
        "expiration": "2017-08-18 13:03:12" // when the code will expire
    }

### Delete [DELETE]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    - type (string, required) - OTP type
    
+ Response 200 (application/json)

    {
        "status": "ok",
        "removedOtps": 1,
    }
    
## Decrement OTP attempts [/user/{userId}/mta/otp/decrement/{type}]

### Example [POST]

+ Headers

    Auth-Token: YourResellerApiToken

+ Parameters

    - userId (number, required) - User id
    - type (string, required) - OTP type
    
+ Response 200 (application/json)

    {
        "status": "ok",
        "updatedOtps": 1,
        "otp": {
          "type":"tfa_email",
          "code":"568139",
          "attempts":"5",
          "expiration":"2017-08-18 13:03:12"
        }
    }